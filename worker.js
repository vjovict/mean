self.addEventListener('message', (e) => {
    let j = 0;
    for (let i = 0; i <= 10000000000; i++) {
        j = i;
    }
    self.postMessage(e.data + ' accessed via javascript worker. "j" is now ' + j + ' by the way!');
    self.close();
})
