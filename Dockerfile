FROM node:10 as mean-app

WORKDIR /usr/src/app

COPY package.json .
RUN npm --proxy http://172.25.30.117:6060 install
COPY . ./
RUN npm run build

FROM nginx:1.12-alpine
COPY nginx.conf etc/nginx/nginx.conf
COPY --from=mean-app /usr/src/app/dist usr/share/nginx/html

EXPOSE 80

CMD ["nginx -g", "daemon off"]
