export interface Message {
    _id: string;
    text: string;
    author: string;
    _v: string;
}

export interface Messages {
    page: number;
    size: number;
    count: number;
    data: Message[];
}
