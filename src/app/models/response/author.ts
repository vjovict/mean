export interface Author {
    _id: string;
    username: string;
    email: string;
}

export interface Authors {
    page: number;
    size: number;
    count: number;
    data: Author[];
}
