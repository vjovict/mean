export class Author {

    constructor(
        public username: string,
        public email: string,
    ) {}
}
