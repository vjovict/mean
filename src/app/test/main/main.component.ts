import { Component, ViewChild, AfterViewInit } from '@angular/core';
import { SubComponent } from '../sub/sub.component';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css'],
  interpolation: ['%', '%']
})
export class MainComponent implements AfterViewInit {

  @ViewChild(SubComponent)
  private timerComponent: SubComponent;

  workerMessage: string;
  workerMessage2: string;
  resetClass = 'paper';

  constructor() { }

  ngAfterViewInit() {
    setTimeout(() => this.seconds = () => this.timerComponent.seconds, 0);

    const worker = new Worker('./../../../../worker.js');
    worker.postMessage('Direct access to hotspot');
    worker.addEventListener('message', (e) => {
      this.workerMessage = e.data;
    });

    const worker2 = new Worker('./../../../../worker2.js');

    worker2.addEventListener('message', (e) => {
      this.workerMessage2 = e.data;
    }, false);
    worker2.postMessage('worker2: ');
  }

  seconds = () => 0;

  start() {
    this.timerComponent.start();
  }

  stop() {
    this.timerComponent.stop();
  }

}
