import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-sub',
  templateUrl: './sub.component.html',
  styleUrls: ['./sub.component.css']
})
export class SubComponent implements OnInit, OnDestroy {

  message = '';
  seconds = 11;
  intervalId = null;

  constructor() { }

  ngOnInit() {
    this.start();
  }

  ngOnDestroy() {
    this.clearTimer();
  }

  start() {
    this.countDown();
  }

  stop() {
    this.clearTimer();
    this.message = `Holding at T-${this.seconds} seconds`;
  }

  clearTimer() {
    clearInterval(this.intervalId);
  }

  countDown() {
    this.clearTimer();
    this.intervalId = setInterval(() => {
      this.seconds -= 1;

      if (this.seconds === 0) {
        this.message = 'Blast off!';
      } else {
        if (this.seconds < 0) {
          this.seconds = 10;
        }
        this.message = `T-${this.seconds} seconds and counting`;
      }
    }, 1000);
  }

}
