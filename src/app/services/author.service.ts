import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Author, Authors } from '../models/response/author';
import { Shared } from '../models/response/shared';

@Injectable({
  providedIn: 'root'
})
export class AuthorService {

  baseUrl = `${environment.baseUrl}/authors`;

  constructor(
    private http: HttpClient
  ) { }

  getAuthors(page: number | string = 1, size: number | string = 10) {
    return this.http.get<Authors>(this.baseUrl + '?page=' + page + '&size=' + size);
  }

  getAuthor(id: string) {
    return this.http.get<Author>(this.baseUrl + '/' + id);
  }

  searchAuthors(page: number | string = 1, size: number | string = 10, query: string = '') {
    return this.http.get<Authors>(this.baseUrl + '/search?q=' + query + '&page=' + page + '&size=' + size);
  }

  saveAuthor(body: object) {
    return this.http.post<Shared>(this.baseUrl, body);
  }

  updateAuthor(body: object, id: string) {
    return this.http.put<Shared>(this.baseUrl + '/' + id, body);
  }

  deleteAuthor(id: string) {
    return this.http.delete<Shared>(this.baseUrl + '/' + id);
  }
}
