import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Messages } from '../models/response/message';
import { Shared } from '../models/response/shared';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  baseUrl = `${environment.baseUrl}/messages`;

  constructor(private http: HttpClient) { }

  getMessages(page: string | number = 1, size: string | number = 10) {
    return this.http.get<Messages>(this.baseUrl + '?page=' + page + '&size=' + size);
  }

  getMessage(id: string) {
    return this.http.get<Messages>(this.baseUrl + '/' + id);
  }

  searchMessage(page: string | number = 1, size: string | number = 10, query: string = '') {
    return this.http.get<Messages>(this.baseUrl + '/search?q=' + query + '&page=' + page + '&size=' + size);
  }

  getAuthorMessages(id: string, page: string | number = 1, size: string | number = 10, query: string = '') {
    return this.http.get<Messages>(this.baseUrl + '/author/' + id + '?q=' + query + '&page=' + page + '&size=' + size);
  }

  saveMessage(body: object) {
    return this.http.post<Shared>(this.baseUrl, body);
  }

  updateMessage(body: object, id: string) {
    return this.http.put<Shared>(this.baseUrl + '/' + id, body);
  }

  deleteMessage(id: string) {
    return this.http.delete<Shared>(this.baseUrl + '/' + id);
  }
}
