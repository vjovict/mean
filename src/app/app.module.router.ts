import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthorComponent } from './author/author.component';
import { MessageComponent } from './message/message.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ShowAuthorComponent } from './author/show-author/show-author.component';
import { MainComponent } from './test/main/main.component';

const appRoutes: Routes = [
    { path: 'authors', component: AuthorComponent },
    { path: 'author/:id', component: ShowAuthorComponent },

    { path: '', redirectTo: '/authors', pathMatch: 'full' },
    { path: '**', component: MainComponent },
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes),
    ],
    exports: [
        RouterModule,
    ],
})
export class AppRoutingModule {}
