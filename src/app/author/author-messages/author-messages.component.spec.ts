import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthorMessagesComponent } from './author-messages.component';

describe('AuthorMessagesComponent', () => {
  let component: AuthorMessagesComponent;
  let fixture: ComponentFixture<AuthorMessagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthorMessagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthorMessagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
