import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { faEnvelopeOpenText, faEdit } from '@fortawesome/free-solid-svg-icons';

import { Message } from '../../models/response/message';
import { MessageService } from '../../services/message.service';

@Component({
  selector: 'app-author-messages',
  templateUrl: './author-messages.component.html',
  styleUrls: ['./author-messages.component.css']
})
export class AuthorMessagesComponent implements OnInit {

  message: Message;
  messages: Message[];
  authorId: string;
  currentPage: number;
  pageSize: number;
  pages: number[];
  isLoading: boolean;
  query = { param: '' };
  showCreateMessage: boolean;
  showEditMessage: boolean;
  faMsg = faEnvelopeOpenText;
  faEdit = faEdit;

  constructor(
    private route: ActivatedRoute,
    private mS: MessageService,
    private toastr: ToastrService,
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(
      (params: ParamMap) => {
        this.authorId = params.get('id');
        this.fetchMessages();
      }
    );
  }

  fetchMessages() {
    this.isLoading = true;
    this.mS.getAuthorMessages(this.authorId, this.currentPage, this.pageSize, this.query.param).subscribe(
      res => {
        this.currentPage = res.page;
        this.pageSize = res.size;

        if (this.currentPage > 1 && (((this.currentPage - 1) * this.pageSize) + 1) > res.count) {
          this.currentPage -= 1;
          this.fetchMessages();
          return;
        }

        this.pages = new Array(Math.ceil(res.count / res.size)).fill(null).map((v, k) => k + 1);
        this.messages = res.data.map(v => ({ ...v, id: v._id }));
        this.isLoading = false;
      },
      err => {
        this.isLoading = false;
        this.toastr.error(err.error.responseMessage, 'Error');
      }
    );
  }

  deleteMessage(id: string) {
    const confirmDelete = confirm('Delete this message?');

    if (confirmDelete) {
      this.mS.deleteMessage(id).subscribe(
        res => {
          this.toastr.success(res.responseMessage);
          this.fetchMessages();
        },
        err => {
          this.toastr.error(err.responseMessage);
        }
      );
    }
  }

  changePageSize(size: number) {
    this.pageSize = size;
    this.fetchMessages();
  }

  changeCurrentPage(page: number) {
    this.currentPage = page;
    this.fetchMessages();
  }

  setShowCreateMessage(bool: boolean) {
    this.showCreateMessage = bool;
  }

  setShowEditMessage(bool: boolean, msg: Message = null) {
    if (msg) {
      this.message = msg;
    }
    this.showEditMessage = bool;
  }

  reload(bool: boolean) {
    if (bool) {
      this.fetchMessages();
    }
  }

}
