import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { faHashtag, faAt } from '@fortawesome/free-solid-svg-icons';
import { ToastrService } from 'ngx-toastr';

import { Author as AuthorReq } from 'src/app/models/request/author';
import { Author as AuthorRes } from 'src/app/models/response/author';
import { AuthorService } from 'src/app/services/author.service';

@Component({
  selector: 'app-edit-author',
  templateUrl: './edit-author.component.html',
  styleUrls: ['./edit-author.component.css']
})
export class EditAuthorComponent implements OnInit {

  faUser = faHashtag;
  faEmail = faAt;
  isUpdating: boolean;
  author: AuthorReq;
  @Input() selectedAuthor: AuthorRes;
  @Output() hideDisplay = new EventEmitter<boolean>();
  @Output() updatedAuthor = new EventEmitter<boolean>();

  constructor(
    private aS: AuthorService,
    private toastr: ToastrService,
  ) { }

  ngOnInit() {
    this.author = new AuthorReq(this.selectedAuthor.username, this.selectedAuthor.email);
  }

  setDisplayEditAuthor(bool: boolean) {
    this.hideDisplay.emit(bool);
  }

  updateAuthor() {
    this.isUpdating = true;
    this.aS.updateAuthor(this.author, this.selectedAuthor._id).subscribe(
      res => {
        this.isUpdating = false;
        this.setDisplayEditAuthor(false);
        this.toastr.success(res.responseMessage, 'Success');
        this.updatedAuthor.emit(true);
      },
      err => {
        this.isUpdating = false;
        const errObj = err.error;
        this.toastr.error(errObj.responseMessage, 'Error');
      }
    );
  }

}
