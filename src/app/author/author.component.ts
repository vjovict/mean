import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { faUserGraduate, faAngleDown, faSearch, faTimesCircle, faSpinner } from '@fortawesome/free-solid-svg-icons';

import { AuthorService } from '../services/author.service';

@Component({
  selector: 'app-author',
  templateUrl: './author.component.html',
  styleUrls: ['./author.component.css']
})
export class AuthorComponent implements OnInit {

  authors: object;
  currentPage: number;
  pageSize: number;
  pages: number[];
  isLoading: boolean;
  showAddAuthor: boolean;
  paginationMsg: string;
  query = { author: '' };
  faQ = faSearch;
  faDel = faTimesCircle;
  faUser = faUserGraduate;
  faAngleDown = faAngleDown;
  faLoader = faSpinner;

  constructor(
    private aS: AuthorService,
    private toastr: ToastrService,
  ) { }

  ngOnInit() {
    this.pageSize = 5;
    this.currentPage = 1;
    this.showAddAuthor = false;
    this.fetchAuthors();
  }

  fetchAuthors() {
    this.isLoading = true;
    this.aS.searchAuthors(this.currentPage, this.pageSize, this.query.author).subscribe(
      res => {
        const authors = res.data;
        this.pageSize = res.size;
        this.currentPage = res.page;

        if (this.currentPage > 1 && (((this.currentPage - 1) * this.pageSize) + 1) > res.count) {
          this.currentPage -= 1;
          this.fetchAuthors();
          return;
        }

        this.pages = Array(Math.ceil(res.count / res.size)).fill(null).map((value, index) => index + 1);
        this.authors = authors.map(author => ({ ...author, id: author._id }));

        const pageStart = ((this.currentPage - 1) * this.pageSize) + 1;
        const pageEnd = Math.min(res.count, (pageStart + this.pageSize - 1));
        this.paginationMsg = `Showing ${pageStart} - ${pageEnd} of ${res.count} records`;

        this.isLoading = false;
      },
      err => {
        this.isLoading = false;
        this.toastr.error(err.error.responseMessage, 'Error');
      }
    );
  }

  deleteAuthor(id: string) {
    const confirmDelete = confirm('Delete this author?');

    if (confirmDelete) {
      this.aS.deleteAuthor(id).subscribe(
        res => {
          this.toastr.success(res.responseMessage);
          this.fetchAuthors();
        },
        err => {
          this.toastr.error(err.responseMessage);
        }
      );
    }
  }

  searchAuthor() {
    this.setCurrentPage(1);
  }

  clearQuery() {
    this.query.author = '';
    this.searchAuthor();
  }

  setPageSize(size: number) {
    this.pageSize = size;
    this.currentPage = 1;
    this.fetchAuthors();
  }

  setCurrentPage(page: number) {
    this.currentPage = page;
    this.fetchAuthors();
  }

  setShowAddAuthor(bool: boolean) {
    this.showAddAuthor = bool;
  }

  reload(event: boolean) {
    if (event) {
      this.fetchAuthors();
    }
  }

}
