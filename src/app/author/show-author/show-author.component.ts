import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

import { AuthorService } from 'src/app/services/author.service';
import { Author } from 'src/app/models/response/author';

@Component({
  selector: 'app-show-author',
  templateUrl: './show-author.component.html',
  styleUrls: ['./show-author.component.css']
})
export class ShowAuthorComponent implements OnInit {

  author: Author;
  authorId: string;
  showEditAuthor: boolean;
  isLoading: boolean;

  constructor(
    private aS: AuthorService,
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(
      (params: ParamMap) => {
        this.authorId = params.get('id');
        this.fetchAuthor();
      }
    );
  }

  fetchAuthor() {
    this.isLoading = true;
    this.aS.getAuthor(this.authorId).subscribe(
      res => {
        this.isLoading = false;
        this.author = res;
      },
      err => {
        this.isLoading = false;
        this.toastr.error(err.error.responseMessage);
      }
    );
  }

  setShowEditAuthor(bool: boolean) {
    this.showEditAuthor = bool;
  }

  reload(bool: boolean) {
    if (bool) {
      this.fetchAuthor();
    }
  }

  goBack() {
    this.router.navigate(['/authors']);
  }

}
