import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { faHashtag, faAt } from '@fortawesome/free-solid-svg-icons';
import { ToastrService } from 'ngx-toastr';
import { Author } from 'src/app/models/request/author';
import { AuthorService } from 'src/app/services/author.service';

@Component({
  selector: 'app-add-author',
  templateUrl: './add-author.component.html',
  styleUrls: ['./add-author.component.css']
})
export class AddAuthorComponent implements OnInit {

  faUser = faHashtag;
  faEmail = faAt;
  isSaving: boolean;
  author: Author;
  @Input() show: boolean;
  @Output() hideDisplay = new EventEmitter<boolean>();
  @Output() savedAuthor = new EventEmitter<boolean>();

  constructor(
    private aS: AuthorService,
    private toastr: ToastrService,
  ) { }

  ngOnInit() {
    this.author = new Author('', '');
  }

  setDisplayAddAuthor(bool: boolean) {
    this.hideDisplay.emit(bool);
  }

  saveAuthor() {
    this.isSaving = true;
    this.aS.saveAuthor(this.author).subscribe(
      res => {
        this.isSaving = false;
        this.author = new Author('', '');
        this.toastr.success(res.responseMessage, 'Success');
        this.savedAuthor.emit(true);
      },
      err => {
        this.isSaving = false;
        const errObj = err.error;
        this.toastr.error(errObj.responseMessage, 'Error');
      }
    );
  }

}
