import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

import { Message } from '../models/response/message';
import { MessageService } from '../services/message.service';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {

  messages: Message[];
  authorId: string;
  currentPage: number;
  pageSize: number;
  pages: number[];
  isLoading: boolean;

  constructor(
    private route: ActivatedRoute,
    private mS: MessageService,
    private toastr: ToastrService,
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(
      (params: ParamMap) => {
        this.authorId = params.get('id');
        this.fetchMessages();
      }
    );
  }

  fetchMessages() {
    this.isLoading = true;
    this.mS.searchMessage(this.currentPage, this.pageSize).subscribe(
      res => {
        this.currentPage = res.page;
        this.pageSize = res.size;
        this.pages = new Array(Math.ceil(res.count / res.size)).fill(null).map((v, k) => k + 1);
        this.messages = res.data.map(v => ({ ...v, id: v._id }));
        this.isLoading = false;
      },
      err => {
        this.isLoading = false;
        this.toastr.error(err.error.responseMessage, 'Error');
      }
    );
  }

  changePageSize(size: number) {
    this.pageSize = size;
    this.fetchMessages();
  }

  changeCurrentPage(page: number) {
    this.currentPage = page;
    this.fetchMessages();
  }

}
