import { Component, OnChanges, EventEmitter, Output, Input } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

import { MessageService } from 'src/app/services/message.service';
import { Message as MessageReq } from 'src/app/models/request/message';
import { Message as MessageRes } from 'src/app/models/response/message';

@Component({
  selector: 'app-edit-message',
  templateUrl: './edit-message.component.html',
  styleUrls: ['./edit-message.component.css']
})
export class EditMessageComponent implements OnChanges {

  isUpdating: boolean;
  newMessage: MessageReq;
  @Input() message: MessageRes;
  @Output() hideDisplay = new EventEmitter<boolean>();
  @Output() updatedMessage = new EventEmitter<boolean>();

  constructor(
    private mS: MessageService,
    private toastr: ToastrService,
  ) { }

  ngOnChanges() {
    this.newMessage = new MessageReq(this.message.text, this.message.author);
  }

  update() {
    this.isUpdating = true;
    this.mS.updateMessage(this.newMessage, this.message._id).subscribe(
      res => {
        this.isUpdating = false;
        this.toastr.success(res.responseMessage, 'Success');
        this.updatedMessage.emit(true);
        this.setDisplay(false);
      },
      err => {
        this.isUpdating = false;
        this.toastr.error(err.error.responseMessage, 'Error');
      }
    );
  }

  setDisplay(bool: boolean) {
    this.hideDisplay.emit(bool);
  }

}
