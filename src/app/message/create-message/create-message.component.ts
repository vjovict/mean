import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

import { MessageService } from 'src/app/services/message.service';
import { Message } from 'src/app/models/request/message';

@Component({
  selector: 'app-create-message',
  templateUrl: './create-message.component.html',
  styleUrls: ['./create-message.component.css']
})
export class CreateMessageComponent implements OnInit {

  isSaving: boolean;
  message: Message;
  @Input() authorId: string;
  @Output() hideDisplay = new EventEmitter<boolean>();
  @Output() savedMessage = new EventEmitter<boolean>();

  constructor(
    private mS: MessageService,
    private toastr: ToastrService,
  ) { }

  ngOnInit() {
    this.message = new Message('', this.authorId);
  }

  save() {
    this.isSaving = true;
    this.mS.saveMessage(this.message).subscribe(
      res => {
        this.isSaving = false;
        this.message = new Message('', this.authorId);
        this.toastr.success(res.responseMessage, 'Success');
        this.savedMessage.emit(true);
      },
      err => {
        this.isSaving = false;
        this.toastr.error(err.error.responseMessage, 'Error');
      }
    );
  }

  setDisplay(bool: boolean) {
    this.hideDisplay.emit(bool);
  }

}
