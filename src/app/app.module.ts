import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ToastrModule } from 'ngx-toastr';

import { AppComponent } from './app.component';
import { AuthorComponent } from './author/author.component';
import { MessageComponent } from './message/message.component';
import { AppRoutingModule } from './app.module.router';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AddAuthorComponent } from './author/add-author/add-author.component';
import { ShowAuthorComponent } from './author/show-author/show-author.component';
import { EditAuthorComponent } from './author/edit-author/edit-author.component';
import { AuthorMessagesComponent } from './author/author-messages/author-messages.component';
import { CreateMessageComponent } from './message/create-message/create-message.component';
import { EditMessageComponent } from './message/edit-message/edit-message.component';
import { MainComponent } from './test/main/main.component';
import { SubComponent } from './test/sub/sub.component';

@NgModule({
  declarations: [
    AppComponent,
    AuthorComponent,
    MessageComponent,
    PageNotFoundComponent,
    AddAuthorComponent,
    ShowAuthorComponent,
    EditAuthorComponent,
    AuthorMessagesComponent,
    CreateMessageComponent,
    EditMessageComponent,
    MainComponent,
    SubComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    FontAwesomeModule,
    ToastrModule.forRoot(),
    BrowserAnimationsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
